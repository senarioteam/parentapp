package com.example.parentapp.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.parentapp.ApiPost;
import com.example.parentapp.Loading;
import com.example.parentapp.Models.AuthSession;
import com.example.parentapp.Models.LoginModel;
import com.example.parentapp.Models.LoginUser;
import com.example.parentapp.Models.ParentLoginRes;
import com.example.parentapp.Models.User;
import com.example.parentapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "as";
    private TextInputLayout email, password;
    private Button btnlogin;
    public static final String MY_PREF = "my pref";
    public String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        email = findViewById(R.id.loginemail);
        password = findViewById(R.id.loginpassword);
        btnlogin = findViewById(R.id.loginbutton);

        getFcmToken();
    }

    private void loginUser(String email, String password, String fcm_token) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        LoginModel loginModel = new LoginModel(email, password, fcm_token);
        Call<ParentLoginRes> call = apiPost.loginUser(loginModel);
        call.enqueue(new Callback<ParentLoginRes>() {
            @Override
            public void onResponse(Call<ParentLoginRes> call, Response<ParentLoginRes> response) {
                if (response.isSuccessful()) {
                    ParentLoginRes resObj = response.body();
                    List<User> list = Collections.singletonList(resObj.getUser());

                    if (resObj.getMessage().equals("Signed in successfully")) {

                        @SuppressLint("CommitPrefEdits")
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREF, MODE_PRIVATE).edit();
                        editor.putString("checkKey", list.get(0).getId().toString());
                        editor.apply();

                        Toast.makeText(LoginActivity.this, "Logged in Successfully", Toast.LENGTH_SHORT).show();
                        Loading.dismiss();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(LoginActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ParentLoginRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login(View view) {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    private void getFcmToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();

                        btnlogin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String emailtext = email.getEditText().getText().toString();
                                String passwordtext = password.getEditText().getText().toString();
                                if (TextUtils.isEmpty(emailtext)) {
                                    email.setError("enter your email");
                                    return;
                                } else {
                                    email.setError(null);
                                }
                                if (TextUtils.isEmpty(passwordtext)) {
                                    password.setError("enter your password");
                                    return;
                                } else {
                                    password.setError(null);
                                }
                                if (passwordtext.length() < 8) {
                                    password.setError("password too short");
                                    return;
                                } else {
                                    password.setError(null);
                                }
                                if (!emailtext.matches(emailpattern)) {
                                    email.setError("invalid email");
                                    return;
                                } else {
                                    email.setError(null);
                                }
                                LoginUser user = new LoginUser(emailtext, passwordtext);
                                AuthSession authSession = new AuthSession(LoginActivity.this);
                                authSession.saveSession(user);
                                loginUser(emailtext, passwordtext, token);
                            }
                        });

//                        // Log and toast
//                        @SuppressLint({"StringFormatInvalid", "LocalSuppress"})
//                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
//                        Toast.makeText(LoginActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }

}