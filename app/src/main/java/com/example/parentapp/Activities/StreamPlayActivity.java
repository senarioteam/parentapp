package com.example.parentapp.Activities;

import static com.example.parentapp.Activities.LoginActivity.MY_PREF;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.example.parentapp.Adapters.AttendRecAdapter;
import com.example.parentapp.ApiPost;
import com.example.parentapp.Loading;
import com.example.parentapp.Models.AttendDatum;
import com.example.parentapp.Models.DriverNameDatum;
import com.example.parentapp.Models.DriverNameRes;
import com.example.parentapp.Models.ChildRecord;
import com.example.parentapp.Models.TripNameRes;
import com.example.parentapp.R;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSourceFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StreamPlayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream_play);
        SharedPreferences preferences = getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
        String parentId = preferences.getString("checkKey", "abc");
        tripName(parentId);
    }

    private void getDriverName(String id) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        DriverNameDatum driverNameDatum = new DriverNameDatum(id);
        Call<DriverNameRes> call = apiPost.getDriverName(driverNameDatum);
        call.enqueue(new Callback<DriverNameRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DriverNameRes> call, Response<DriverNameRes> response) {
                if (response.isSuccessful()) {


                } else {
                    Toast.makeText(StreamPlayActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DriverNameRes> call, Throwable t) {
                Toast.makeText(StreamPlayActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void tripName(String parent_id) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<TripNameRes> call = apiPost.tripName(parent_id);
        call.enqueue(new Callback<TripNameRes>() {
            @Override
            public void onResponse(Call<TripNameRes> call, Response<TripNameRes> response) {
                if (response.isSuccessful()) {
                    Loading.dismiss();
                    BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                    TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
                    TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
                    SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(StreamPlayActivity.this, trackSelector);

                    PlayerView playerView = findViewById(R.id.simple_player);

                    playerView.setPlayer(player);

                    // Create RTMP Data Source
                    RtmpDataSourceFactory rtmpDataSourceFactory = new RtmpDataSourceFactory();

                    MediaSource videoSource = new ExtractorMediaSource
                            .Factory(rtmpDataSourceFactory)
                            .createMediaSource(Uri.parse("rtmp://162.240.21.77:1935/live/" + response.body().getTripName()));

                    player.prepare(videoSource);
                    player.setPlayWhenReady(true);
                } else {
                    Loading.dismiss();
                    Toast.makeText(StreamPlayActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TripNameRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(StreamPlayActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}