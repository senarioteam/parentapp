package com.example.parentapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.parentapp.Models.AttendDatum;
import com.example.parentapp.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AttendRecAdapter extends RecyclerView.Adapter<AttendRecAdapter.ViewHolder> {
    private ArrayList<AttendDatum> datumArrayList = new ArrayList<>();
    private Context context;

    public AttendRecAdapter(ArrayList<AttendDatum> datumArrayList, Context context) {
        this.datumArrayList = datumArrayList;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.attend_rec_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.attendance.setText(datumArrayList.get(position).getAttendance());
        holder.date.setText(datumArrayList.get(position).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView attendance, date;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            attendance = itemView.findViewById(R.id.attendance);
            date = itemView.findViewById(R.id.attend_createdat);

        }
    }
}
