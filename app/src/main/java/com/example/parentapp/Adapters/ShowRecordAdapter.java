package com.example.parentapp.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.parentapp.Models.Datum;
import com.example.parentapp.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ShowRecordAdapter extends RecyclerView.Adapter<ShowRecordAdapter.ViewHolder> {
    private ArrayList<Datum> datumArrayList = new ArrayList<>();
    private Context context;

    public ShowRecordAdapter(ArrayList<Datum> datumArrayList, Context context) {
        this.datumArrayList = datumArrayList;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_rec_layout, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

        holder.name.setText(datumArrayList.get(position).getChildsName());
        holder.location.setText(datumArrayList.get(position).getLocation());
        holder.status.setText(datumArrayList.get(position).getStatus());
        holder.create_time.setText(datumArrayList.get(position).getCreatedAt());
        if (datumArrayList.get(position).getStatus().equalsIgnoreCase("Dropped off")){
            holder.create_time.setTextColor(Color.parseColor("#DD1F1F"));
        }
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,location,status,create_time;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            location = itemView.findViewById(R.id.location);
            status = itemView.findViewById(R.id.status);
            create_time = itemView.findViewById(R.id.createdat);

        }
    }
}
