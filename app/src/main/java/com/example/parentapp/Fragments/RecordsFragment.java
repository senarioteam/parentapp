package com.example.parentapp.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.parentapp.ApiPost;
import com.example.parentapp.Loading;
import com.example.parentapp.Models.ChildRecord;
import com.example.parentapp.Models.Datum;
import com.example.parentapp.Models.DelRecInputs;
import com.example.parentapp.Models.ParentLoginRes;
import com.example.parentapp.Models.ShowRecordsRes;
import com.example.parentapp.R;
import com.example.parentapp.Adapters.ShowRecordAdapter;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.parentapp.Activities.LoginActivity.MY_PREF;

public class RecordsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ShowRecordAdapter showRecordAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_records, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swiperecords);

        SharedPreferences preferences = getActivity().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
        String parentId = preferences.getString("checkKey", "abc");
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                enlistRecords(parentId);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        enlistRecords(parentId);
        return view;
    }

    private void enlistRecords(String id) {
        Loading.show(getActivity());
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecord childRecord = new ChildRecord(id);
        Call<ShowRecordsRes> call = apiPost.showRecords(childRecord);
        call.enqueue(new Callback<ShowRecordsRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ShowRecordsRes> call, Response<ShowRecordsRes> response) {
                if (response.isSuccessful()) {
                    Loading.dismiss();
                    ShowRecordsRes resObj = response.body();
                    List<Datum> list = resObj.getData();
                    showRecordAdapter = new ShowRecordAdapter((ArrayList<Datum>) list, getContext());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    Loading.dismiss();
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(showRecordAdapter);
                    showRecordAdapter.notifyDataSetChanged();

                    //below three lines are for getting first record from the list
//                    List<Datum> list = resObj.getData();
//                    List<Datum> list1 = new ArrayList<>();
//                    list1.add(list.get(0));

//                    if (resObj.getSuccess().equals("true")) {
//                        Toast.makeText(getContext(), list.get(0).getChildsName(), Toast.LENGTH_SHORT).show();
//                        for (int i = 0; i < list.size(); i++) {
//                            try {

//                            } catch (Exception e) {
//                                Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowRecordsRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull @NotNull Menu menu, @NonNull @NotNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.deleteitem_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull @NotNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_record:
                SharedPreferences preferences = getContext().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
                String parentId = preferences.getString("checkKey", "abc");
                deletePreviousRecords(parentId);
                Snackbar.make(getView(), "5 Old Records Deleted", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                break;
        }
        return true;
    }
    private void deletePreviousRecords(String id) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        DelRecInputs delRecInputs = new DelRecInputs(id);
        Call<ParentLoginRes> call = apiPost.deletePreviousRecords(delRecInputs);
        call.enqueue(new Callback<ParentLoginRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ParentLoginRes> call, Response<ParentLoginRes> response) {
                if (response.isSuccessful()) {
                    ParentLoginRes resObj = response.body();
                    if (resObj.getSuccess().equals("true")) {

                    }
                } else {
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ParentLoginRes> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getContext(), "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(getContext(), "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}