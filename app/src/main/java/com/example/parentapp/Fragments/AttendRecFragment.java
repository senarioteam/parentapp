package com.example.parentapp.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.parentapp.Adapters.AttendRecAdapter;
import com.example.parentapp.Adapters.ShowRecordAdapter;
import com.example.parentapp.ApiPost;
import com.example.parentapp.Loading;
import com.example.parentapp.Models.AttendDatum;
import com.example.parentapp.Models.AttendRecRes;
import com.example.parentapp.Models.ChildRecord;
import com.example.parentapp.Models.Datum;
import com.example.parentapp.Models.ShowRecordsRes;
import com.example.parentapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.parentapp.Activities.LoginActivity.MY_PREF;

public class AttendRecFragment extends Fragment {
    private RecyclerView recyclerView;
    private AttendRecAdapter attendRecAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attend_rec, container, false);
        recyclerView = view.findViewById(R.id.attend_rv);
        swipeRefreshLayout = view.findViewById(R.id.swipeattendrecords);

        SharedPreferences preferences = getActivity().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
        String parentId = preferences.getString("checkKey", "abc");
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                enlistAttendanceRec(parentId);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        enlistAttendanceRec(parentId);
        return view;
    }

    private void enlistAttendanceRec(String id) {
        Loading.show(getActivity());
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecord childRecord = new ChildRecord(id);
        Call<AttendRecRes> call = apiPost.showAttendanceRecord(childRecord);
        call.enqueue(new Callback<AttendRecRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<AttendRecRes> call, Response<AttendRecRes> response) {
                if (response.isSuccessful()) {
                    Loading.dismiss();
                    AttendRecRes resObj = response.body();
                    List<AttendDatum> list = resObj.getData();
//                    Toast.makeText(getContext(), list.get(0).getChildsName(), Toast.LENGTH_SHORT).show();
                    attendRecAdapter = new AttendRecAdapter((ArrayList<AttendDatum >) list, getContext());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    Loading.dismiss();
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(attendRecAdapter);
                    attendRecAdapter.notifyDataSetChanged();

                } else {
                    Loading.dismiss();
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AttendRecRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}