package com.example.parentapp.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parentapp.Activities.StreamPlayActivity;
import com.example.parentapp.ApiPost;
import com.example.parentapp.Loading;
import com.example.parentapp.Models.ChildRecord;
import com.example.parentapp.Models.CurrentDropRes;
import com.example.parentapp.Models.CurrentPickDatum;
import com.example.parentapp.Models.CurrentPickRes;
import com.example.parentapp.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.parentapp.Activities.LoginActivity.MY_PREF;


public class HomeFragment extends Fragment {
    private static final String PREF = "pref";
    Button trackLocation,liveVideo;
    TextView tvname, tvpickupTime, tvlocation, tvstatus, tvtraveltime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        trackLocation = view.findViewById(R.id.trackLocation);
        liveVideo = view.findViewById(R.id.liveVideo);
        tvname = view.findViewById(R.id.childName);
        tvpickupTime = view.findViewById(R.id.childTime);
        tvlocation = view.findViewById(R.id.childLocation);
        tvstatus = view.findViewById(R.id.childStatus);
        tvtraveltime = view.findViewById(R.id.childtotaltime);

        SharedPreferences preferences = getActivity().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
        String parentId = preferences.getString("checkKey", "abc");

        SharedPreferences pref = getActivity().getSharedPreferences(PREF, Context.MODE_PRIVATE);
        String status = pref.getString("status_value","abc");
        trackLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://sts.mobifinplus.com/track_vehicle_blade?id=67";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        liveVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (status.equals("Dropped off")) {
//                    Snackbar.make(v, "Your child is not on the trip", Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();
//                } else {
                    startActivity(new Intent(getContext(), StreamPlayActivity.class));
//                }
            }
        });
        currentPickRecord(parentId);
        return view;
    }

    private void currentPickRecord(String id) {
        Loading.show(getActivity());
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecord childRecord = new ChildRecord(id);
        Call<CurrentPickRes> call = apiPost.currentPickRecord(childRecord);
        call.enqueue(new Callback<CurrentPickRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<CurrentPickRes> call, Response<CurrentPickRes> response) {
                if (response.isSuccessful()) {
                    CurrentPickRes resObj = response.body();
                    List<CurrentPickDatum> list = resObj.getData();
                    try {
                        Loading.dismiss();

                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(PREF, Context.MODE_PRIVATE).edit();
                        editor.putString("status_value", list.get(0).getStatus());
                        editor.apply();


                        tvname.setText("Name: " + list.get(0).getChildsName());
                        tvpickupTime.setText("Pickup Time: " + list.get(0).getCreatedAt());
                        tvlocation.setText("Location: " + list.get(0).getLocation());
                        tvstatus.setText("Status: " + list.get(0).getStatus());
                        tvtraveltime.setText("Trip Time: " + "Traveling now");
                    } catch (Exception e) {
                        Loading.dismiss();
                        SharedPreferences preferences = getActivity().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
                        String parentId = preferences.getString("checkKey", "abc");
                        currentDropRecord(parentId);
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CurrentPickRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getContext(), "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(getContext(), "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void currentDropRecord(String id) {
        Loading.show(getActivity());
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecord childRecord = new ChildRecord(id);
        Call<CurrentDropRes> call = apiPost.currentDropRecord(childRecord);
        call.enqueue(new Callback<CurrentDropRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<CurrentDropRes> call, Response<CurrentDropRes> response) {
                if (response.isSuccessful()) {
                    CurrentDropRes resObj = response.body();

//                    Toast.makeText(getContext(), list.get(0).getChildsName(), Toast.LENGTH_SHORT).show();
                    try {
                        Loading.dismiss();
                        tvname.setText("Name: " + resObj.getChildsName());
                        tvpickupTime.setText("Time: " + resObj.getCreatedAt());
                        tvlocation.setText("Location: " + resObj.getLocation());
                        tvstatus.setText("Status: " + resObj.getStatus());
                        tvtraveltime.setText("Trip Time: " + resObj.getTripTime());
                    } catch (Exception e) {
                        Loading.dismiss();
                    }

                } else {
                    Loading.dismiss();
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CurrentDropRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getContext(), "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(getContext(), "Check you internet connection", Toast.LENGTH_SHORT).show();
                }            }
        });
    }
}