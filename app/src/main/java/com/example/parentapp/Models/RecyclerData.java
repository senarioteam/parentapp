package com.example.parentapp.Models;

public class RecyclerData {
    private String id;
    private String user_id;
    private String childs_name;
    private String location;
    private String status;
    private String trip_time;

    public RecyclerData(String user_id) {
        this.user_id = user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChilds_name() {
        return childs_name;
    }

    public void setChilds_name(String childs_name) {
        this.childs_name = childs_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTrip_time() {
        return trip_time;
    }

    public void setTrip_time(String trip_time) {
        this.trip_time = trip_time;
    }
}
