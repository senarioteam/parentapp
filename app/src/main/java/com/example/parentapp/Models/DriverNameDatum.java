package com.example.parentapp.Models;

import com.google.gson.annotations.SerializedName;

public class DriverNameDatum {
    @SerializedName("parent_id")
    private String parent_id;

    public DriverNameDatum(String parent_id) {
        this.parent_id = parent_id;
    }
}
