package com.example.parentapp.Models;

import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;


public class ChildRecord {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("success")
    private String success;
    @SerializedName("data")
    private JsonArray data;

    public ChildRecord(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public JsonArray getData() {
        return data;
    }

    public void setData(JsonArray data) {
        this.data = data;
    }
}
