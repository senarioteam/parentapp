package com.example.parentapp.Models;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("message")
    private String message;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("fcm_token")
    private String fcm_token;


    public LoginModel(String email, String password, String fcm_token) {
        this.email = email;
        this.password = password;
        this.fcm_token = fcm_token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
