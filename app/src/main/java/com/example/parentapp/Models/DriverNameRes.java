package com.example.parentapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverNameRes {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("driver_id")
    @Expose
    private Integer driverId;
    @SerializedName("driver_name")
    @Expose
    private String driverName;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }
}
