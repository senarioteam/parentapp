package com.example.parentapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationDatum {
    @SerializedName("driver_lat")
    @Expose
    private Double driverLat;
    @SerializedName("driver_long")
    @Expose
    private Double driverLong;

    public Double getDriverLat() {
        return driverLat;
    }

    public void setDriverLat(Double driverLat) {
        this.driverLat = driverLat;
    }

    public Double getDriverLong() {
        return driverLong;
    }

    public void setDriverLong(Double driverLong) {
        this.driverLong = driverLong;
    }
}
