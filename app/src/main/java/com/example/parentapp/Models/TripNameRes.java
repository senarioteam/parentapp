package com.example.parentapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripNameRes {
        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("driver_id")
        @Expose
        private Integer driverId;
        @SerializedName("trip_name")
        @Expose
        private String tripName;
        @SerializedName("created_at")
        @Expose
        private String createdAt;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getDriverId() {
            return driverId;
        }

        public void setDriverId(Integer driverId) {
            this.driverId = driverId;
        }

        public String getTripName() {
            return tripName;
        }

        public void setTripName(String tripName) {
            this.tripName = tripName;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }


}
