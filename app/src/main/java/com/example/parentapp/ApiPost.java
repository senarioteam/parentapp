package com.example.parentapp;

import com.example.parentapp.Models.AttendRecRes;
import com.example.parentapp.Models.ChildRecord;
import com.example.parentapp.Models.CurrentDropRes;
import com.example.parentapp.Models.CurrentPickRes;
import com.example.parentapp.Models.DelRecInputs;
import com.example.parentapp.Models.DriverNameDatum;
import com.example.parentapp.Models.DriverNameRes;
import com.example.parentapp.Models.LoginModel;
import com.example.parentapp.Models.ParentLoginRes;
import com.example.parentapp.Models.ShowRecordsRes;
import com.example.parentapp.Models.TripNameRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiPost {
    @POST("parent_sign_in")
    Call<ParentLoginRes> loginUser(@Body LoginModel loginModel);

    @POST("show_records")
    Call<ShowRecordsRes> showRecords(@Body ChildRecord childRecord);

    @POST("on_going_parent")
    Call<CurrentPickRes> currentPickRecord(@Body ChildRecord childRecord);

    @POST("dropoff_parent")
    Call<CurrentDropRes> currentDropRecord(@Body ChildRecord childRecord);

    @POST("delete_records")
    Call<ParentLoginRes> deletePreviousRecords(@Body DelRecInputs delRecInputs);

    @POST("show_attendance")
    Call<AttendRecRes> showAttendanceRecord(@Body ChildRecord childRecord);

    @POST("driver_name_id")
    Call<DriverNameRes> getDriverName(@Body DriverNameDatum driverNameDatum);


    @FormUrlEncoded
    @POST("get_trip_name")
    Call<TripNameRes> tripName(@Field("parent_id") String driver_id);
}
